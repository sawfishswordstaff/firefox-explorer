# BROKEN IN FIREFOX >= 89

# Firefox Explorer <!-- omit in toc -->
Make Firefox look like Internet Explorer 11

> Only tested on Windows 10 Firefox 87.0 (64bit)

![Firefox Explorer preview blank page](./docs/preview.png)
![Firefox Explorer preview google.com](./docs/preview2.png)
![Firefox Explorer preview duckduckgo.com](./docs/preview3.png)

# Contents <!-- omit in toc -->
- [Features](#features)
- [Usage](#usage)
    - [Bookmark Toolbar](#bookmark-toolbar)
- [Install](#install)
- [See Also](#see-also)
    - [Mentions](#mentions)
    - [License](#license)

# Features
- This project tries to make Firefox resemble more like Internet Explorer [11], though the colors and dimensions aren't completely accurate.
- A new tab page icon that I made in a few minutes using inkscape.
- Tab bar below the address bar
- (Optional) Bookmark Toolbar support
- A few more things to note:
    - There is no dark mode version
    - COLORS AND DIMENSIONS ARE NOT ENTIRELY ACCURATE

# Usage
Install it and it will work.

## Bookmark Toolbar
To enable bookmark toolbar, delete the line `display: none !important` under `#PersonalToolbar {` in `userChrome.css`

OR you can simply turn it off under the library menu I think

# Install
1. In a new tab, type or paste `about:config` in the address bar and press Enter/Return. Click the button accepting the risk.
2. In the search box above the list, type or paste userprof and pause while the list is filtered. If you do not see anything on the list, please ignore the rest of these instructions. You can close this tab now.
3. Double-click the `toolkit.legacyUserProfileCustomizations.stylesheets` preference to switch the value from false to true.
4. Type in `about:support` in the address bar and find `Profile Folder`. Go to that directory or simply click `Open Folder` button.
5. Find `chrome` folder. If it isn't there, then create a `chrome` folder in the profile folder.
6. Copy contents in this repository's `chrome` folder (Both `userChrome.css` and `newtab.svg` AND whatever else that may be in there...) and place them in your `chrome` folder

To download this repository, click on the `Code` button at the top of the page and click on `Download ZIP`. Extract the contents into your chrome folder.

OR you could do a git clone

For more information, check out [this page](https://www.userchrome.org/how-create-userchrome-css.html)

# See Also
## Mentions
- [Modified version of this: Place tab bar below address bar](https://old.reddit.com/r/FirefoxCSS/comments/em9t4y/tabs_below_address_bar/)
- [userChrome.org site](https://www.userchrome.org/how-create-userchrome-css.html)

## License
[0BSD](./LICENSE)
